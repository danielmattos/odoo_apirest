# -*- encoding: utf-8 -*-
import json
import requests

api_url = 'http://localhost:5000/api_rest/res.users/1,40/id,name,login'
odoo_url = 'http://localhost:8069'
odoo_user_name = 'admin'  # the user
odoo_user_pass = 'admin'  # the password of the user
database = 'ITALTEL_201611'  # the database


def client():
    # Parametros de conexión
    headers = {
        'content-type': 'application/json',
        'odoo_url': odoo_url,
        'odoo_db': database,
    }
    data = json.dumps({})

    r = requests.get(
        url=api_url,
        data=data,
        auth=(odoo_user_name, odoo_user_pass),
        headers=headers)

    if 400 <= r.status_code < 500:
        print r.status_code
    elif 200 <= r.status_code < 300:
        print r.status_code
        res = json.loads(r.text)
        print res


if __name__ == '__main__':
    client()

# url = 'http://localhost:8069'
# username = 'soporte@peruswfactory.com'  # the user
# pwd = 'gamaliel'  # the password of the user
# dbname = 'ITALTEL_201611'  # the database