import json

from flask import Flask, jsonify
from flask import request, Response
from functools import wraps
from erppeek import Client


def check_auth(username, password, odoo_url=None, odoo_db=None):
    try:
        erppeek_client = Client(odoo_url, odoo_db, username, password)
    except Exception as e:
        erppeek_client = None

    return bool(erppeek_client), erppeek_client


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        print 'Headers' * 30
        print request.headers
        odoo_url = request.headers.get('odoo_url', None)
        odoo_db = request.headers.get('odoo_db', None)

        auth = request.authorization
        if not auth:
            return authenticate()

        check, erppeek_client = check_auth(
            auth.username, auth.password, odoo_url, odoo_db)

        if not check:
            return authenticate()

        kwargs['erppeek_client'] = erppeek_client
        return f(*args, **kwargs)
    return decorated


app = Flask(__name__)


@app.route('/api_rest/', methods=['GET'])
@requires_auth
def indexf(*args, **kwargs):
    return json.dumps({'message': "odoo api rest"})


@app.route(
    '/api_rest/<string:model_name>/',
    methods=['GET'])
@requires_auth
def get_objects_filter1(model_name, *args, **kwargs):
    return get_objects_filter(model_name, *args, **kwargs)


@app.route(
    '/api_rest/<string:model_name>/<string:domain>/',
    methods=['GET'])
@requires_auth
def get_objects_filter2(model_name, domain, *args, **kwargs):
    return get_objects_filter(model_name, domain, *args, **kwargs)


@app.route(
    '/api_rest/<string:model_name>/<string:domain>/<string:fields>/',
    methods=['GET'])
@requires_auth
def get_objects_filter(model_name, domain=None, fields=None, *args, **kwargs):
    if domain is None:
        domain = []
    if str(domain).isdigit():
        domain = [('id', '=', int(domain))]
    if ',' in domain and \
       all([str_id.isdigit() for str_id in domain.split(',')] or [False]):
        try:
            ids = [int(str_id) for str_id in domain.split(',')]
        except ValueError, e:
            return json.dumps('Error en filtro %s' % domain)
        domain = [('id', 'in', ids)]

    if fields is None or fields == '*':
        fields = []
    elif ',' in fields:
        fields = [field.strip()
                  for field in fields.split(',') if field and field.strip()]
    else:
        return json.dumps('Error en fields %s' % fields)

    erppeek_client = kwargs['erppeek_client']
    print 'model_name', model_name
    print 'domain', domain
    print 'fields', fields
    res = erppeek_client.read('res.users', domain, fields=fields)
    return json.dumps(res)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
